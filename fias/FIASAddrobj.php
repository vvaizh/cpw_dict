<?php

class FIASAddrobj extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'fias_addrobj';
	}
	
	public function rules()
	{
		return array();
	}

	public function relations()
	{
		return array();
	}

	public function getDbConnection(){
        return Yii::app()->db_fias;
    }

	public function attributeLabels()
	{
		return array(
			'ACTSTATUS' => 'ACTSTATUS',
			'AOGUID' => 'AOGUID',
			'AOID' => 'AOID',
			'AOLEVEL' => 'AOLEVEL',
			'AREACODE' => 'AREACODE',
			'AUTOCODE' => 'AUTOCODE',
			'CENTSTATUS' => 'CENTSTATUS',
			'CITYCODE' => 'CITYCODE',
			'CODE' => 'CODE',
			'CURRSTATUS' => 'CURRSTATUS',
			'ENDDATE' => 'ENDDATE',
			'FORMALNAME' => 'FORMALNAME',
			'IFNSFL' => 'IFNSFL',
			'IFNSUL' => 'IFNSUL',
			'NEXTID' => 'NEXTID',
			'OFFNAME' => 'OFFNAME',
			'OKATO' => 'OKATO',
			'OKTMO' => 'OKTMO',
			'OPERSTATUS' => 'OPERSTATUS',
			'PARENTGUID' => 'PARENTGUID',
			'PLACECODE' => 'PLACECODE',
			'PLAINCODE' => 'PLAINCODE',
			'POSTALCODE' => 'POSTALCODE',
			'PREVID' => 'PREVID',
			'REGIONCODE' => 'REGIONCODE',
			'SHORTNAME' => 'SHORTNAME',
			'STARTDATE' => 'STARTDATE',
			'STREETCODE' => 'STREETCODE',
			'TERRIFNSFL' => 'TERRIFNSFL',
			'TERRIFNSUL' => 'TERRIFNSUL',
			'UPDATEDATE' => 'UPDATEDATE',
			'CTARCODE' => 'CTARCODE',
			'EXTRCODE' => 'EXTRCODE',
			'SEXTCODE' => 'SEXTCODE',
			'LIVESTATUS' => 'LIVESTATUS',
			'NORMDOC' => 'NORMDOC',
			'FullPath' => 'FullPath',
			'IsTerminal' => 'IsTerminal',
		);
	}

	public function search()
	{
		return new CActiveDataProvider();
	}
}	