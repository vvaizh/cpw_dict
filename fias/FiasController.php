<?php

class FiasController extends Controller
{

	public $recordsAtList = 20;

	public function beforeAction($action = null)
	{
		$contentType = 'application/javascript';

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET");
		header('Content-Type: ' . $contentType);

		return true;
	}

	private function response($data = null)
	{
		if (!$data) return false;
		$response = '';
		$fn = Yii::app()->request->getParam('callback');
		$response .= CJSON::encode($data);
		if (!empty($fn))
			$response = $fn . '(' . $response . ')';
		echo $response;
	}

	// mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Удм%" AND t.AOLEVEL=1 AND t.ACTSTATUS=1 limit 10\G
	public function actionRegions()
	{
		$res= array();
		if (isset($_GET['term']))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.FORMALNAME LIKE :term AND t.AOLEVEL=1 AND t.ACTSTATUS=1';
			$criteria->params= array(':term' => $_GET['term'].'%');
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res[] = array(
					'label'      => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value'      => $item['FORMALNAME'],
					'AOID'       => $item['AOID'],
					'SHORTNAME'  => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE']);
			}
		}
		$this->response($res);
	}

	// mysql> select * from fias_addrobj where aolevel="3" and regioncode="18" and formalname like "Ал%" limit 3\G
	public function actionArea()
	{
		$res= array();
		if (isset($_GET['region']) && $_GET['region']['REGIONCODE'] && isset($_GET['area']))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=3 AND t.ACTSTATUS=1';
			$criteria->params= array
			(
				':term'        => $_GET['area'].'%',
				':regioncode'  => $_GET['region']['REGIONCODE']
			);
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res[] = array(
					'label'      => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value'      => $item['FORMALNAME'],
					'AOID'       => $item['AOID'],
					'SHORTNAME'  => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],

					'AREACODE'   => $item['AREACODE']);
			}
		}
		$this->response($res);
	}

	// mysql> select * from fias_addrobj where aolevel="6" and regioncode="18" and formalname like "Завьялово%" limit 3\G
	public function actionPlace()
	{
		$res= array();
		if (isset($_GET['region']) && $_GET['region']['REGIONCODE'] && isset($_GET['place']))
		{
			$condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=6 AND t.ACTSTATUS=1';
			$params= array
			(
				':term' => $_GET['place'].'%',
				':regioncode' => $_GET['region']['REGIONCODE']
			);
			if (isset($_GET['city']) && $_GET['city'] && $_GET['city']['CITYCODE']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.CITYCODE=:citycode';
				$params['citycode']= $_GET['city']['CITYCODE'];
			}
			if (isset($_GET['area']) && $_GET['area'] && $_GET['area']['AREACODE'])
			{
				$condition.= ' AND t.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			$criteria = new CDbCriteria;
			$criteria->params= $params;
			$criteria->condition = $condition;
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res[] = array(
					'label'      => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value'      => $item['FORMALNAME'],
					'AOID'       => $item['AOID'],
					'SHORTNAME'  => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],

					'AREACODE'   => $item['AREACODE'],
					'CITYCODE'   => $item['CITYCODE'],
					'PLACECODE'  => $item['PLACECODE']);
			}
		}
		$this->response($res);
	}

	// mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Ижев%" AND t.AOLEVEL=4 AND t.ACTSTATUS=1 limit 1\G
	public function actionCity()
	{
		$res= array();
		if (isset($_GET['city']))
		{
			$params = array(':term' => $_GET['city'].'%');
			if (isset($_GET['region']) && $_GET['region']['REGIONCODE']) {
				$condition = 'city.FORMALNAME LIKE :term AND city.REGIONCODE=:regioncode AND city.AOLEVEL=4 AND city.ACTSTATUS=1';
				$params['regioncode'] = $_GET['region']['REGIONCODE'];
			} else 
				$condition = "city.FORMALNAME LIKE :term AND (city.AOLEVEL=4 OR (city.AOLEVEL=1 AND city.REGIONCODE in ('77', '78'))) AND city.ACTSTATUS=1";
			
			if (isset($_GET['area']) && $_GET['area'] && $_GET['area']['AREACODE']) // Если есть район - выбираем города для текущего района
			{
				$condition .= ' AND city.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			$objects = Yii::app()->db_fias->createCommand()
			    ->select('city.*, region.FORMALNAME REGIONNAME, region.SHORTNAME RSHORTNAME')
			    ->from    ('fias_addrobj city')
			    ->leftJoin('fias_addrobj region', 'city.REGIONCODE=region.REGIONCODE AND region.AOLEVEL=1')
			    ->where($condition, $params)
			    ->queryAll();
			foreach ($objects as $item)
			{
				$res[] = array(
					'label'      => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value'      => $item['FORMALNAME'],
					'AOID'       => $item['AOID'],
					'SHORTNAME'  => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],

					'AREACODE'   => $item['AREACODE'],
					'CITYCODE'   => $item['CITYCODE'],

					'REGIONNAME' => $item['RSHORTNAME'] . ' ' . $item['REGIONNAME'],
					'AOLEVEL'    => $item['AOLEVEL']);
			}
		}
		$this->response($res);
	}

	// mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Лихвинцев%" AND t.AOLEVEL=7 AND t.ACTSTATUS=1 limit 1\G
	public function actionStreet()
	{
		$res= array();
		$isset_place_or_city = isset($_GET['place']) || isset($_GET['city']);
		if (isset($_GET['region']) && isset($_GET['street']) && $isset_place_or_city && $_GET['region']['REGIONCODE'])
		{
			$condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=7 AND t.ACTSTATUS=1';
			$params= array
			(
				':term'       => '%' . $_GET['street'] . '%',
				':regioncode' => $_GET['region']['REGIONCODE']
			);
			if (isset($_GET['area']) && $_GET['area'] && $_GET['area']['AREACODE']) // Если есть район - выбираем города для текущего района
			{
				$condition.= ' AND t.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			if (isset($_GET['city']) && $_GET['city'] && $_GET['city']['CITYCODE']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.CITYCODE=:citycode';
				$params['citycode']= $_GET['city']['CITYCODE'];
			}
			if (isset($_GET['place']) && $_GET['place'] && $_GET['place']['PLACECODE']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.PLACECODE=:placecode';
				$params['placecode']= $_GET['place']['PLACECODE'];
			}
			$criteria = new CDbCriteria;
			$criteria->params= $params;
			$criteria->condition = $condition;
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res[] = array(
					'label'      => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value'      => $item['FORMALNAME'],
					'AOID'       => $item['AOID'],
					'SHORTNAME'  => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],

					'AREACODE'   => $item['AREACODE'],
					'CITYCODE'   => $item['CITYCODE'],
					'PLACECODE'  => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE'],
					'FullPath'   => $item['FullPath']);
			}
		}
		$this->response($res);
	}

	function getRegionByCode($region_code)
	{
		$res= '';
		if (isset($region_code) && $region_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '" AND t.AOLEVEL=1 AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE']);
			}
		}
		return $res;
	}

	function getAreaByCode($region_code, $area_code)
	{
		$res= '';
		if (isset($area_code) && $area_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.AREACODE="' . $area_code . '"'
				. ' AND t.AOLEVEL=3 AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE']);
		}
		return $res;
	}

	function getCityByCode($region_code, $area_code, $city_code)
	{
		$res= '';
		if (isset($city_code) && $city_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.CITYCODE="' . $city_code . '"'
				. ' AND t.AOLEVEL=4 AND t.ACTSTATUS=1';
			if (isset($area_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.AREACODE="' . $area_code . '"';
			}
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE']);
		}
		return $res;
	}

	function getPlaceByCode($region_code, $area_code, $city_code, $place_code)
	{
		$res= '';
		if (isset($place_code) && $place_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.PLACECODE="' . $place_code . '"'
				. ' AND t.AOLEVEL=6 AND t.ACTSTATUS=1';
			if (isset($area_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.AREACODE="' . $area_code . '"';
			}
			if (isset($city_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.CITYCODE="' . $city_code . '"';
			}
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE']);
		}
		return $res;
	}

	public function getAddressByStreet($item)
	{
		$object_street = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE'],
			'PLACECODE' => $item['PLACECODE'],
			'STREETCODE' => $item['STREETCODE']);
		$object_region = $this->getRegionByCode($object_street['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_street['REGIONCODE'], $object_street['AREACODE']);
			$object_city = $this->getCityByCode(
				$object_street['REGIONCODE'],
				$object_street['AREACODE'],
				$object_street['CITYCODE']);
			$object_place = $this->getPlaceByCode(
				$object_street['REGIONCODE'],
				$object_street['AREACODE'],
				$object_street['CITYCODE'],
				$object_street['PLACECODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city,
			'object_place' => $object_place,
			'object_street' => $object_street
		);
		return CJSON::encode($res);
	}

	public function getAddressByPlace($item)
	{
		$object_place = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE'],
			'PLACECODE' => $item['PLACECODE']);
		$object_region = $this->getRegionByCode($object_place['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_place['REGIONCODE'], $object_place['AREACODE']);
			$object_city = $this->getCityByCode(
				$object_place['REGIONCODE'],
				$object_place['AREACODE'],
				$object_place['CITYCODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city,
			'object_place' => $object_place
		);
		return CJSON::encode($res);
	}

	public function getAddressByCity($item)
	{
		$object_city = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE']);
		$object_region = $this->getRegionByCode($object_city['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_city['REGIONCODE'], $object_city['AREACODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city
		);
		return CJSON::encode($res);
	}

	public function getAddressByArea($item)
	{
		$object_area = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE']);
		$object_region = $this->getRegionByCode($object_area['REGIONCODE']);
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area
		);
		return CJSON::encode($res);
	}

	public function getAddressByRegion($item)
	{
		$object_region = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE']);
		$res = array(
			'object_region' => $object_region
		);
		return CJSON::encode($res);
	}

	public function getStreetByAOID($aoid)
	{
		$res= '';
		if (isset($aoid))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.AOID=:aoid AND t.ACTSTATUS=1 AND t.AOLEVEL=7 ';
			$criteria->params= array(':aoid' => $aoid);
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'AOLEVEL' => $item['AOLEVEL'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE']);
		}
		return $res;
	}

	public function getObjectByAOID($aoid)
	{
		$res= '';
		if (isset($aoid))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.AOID="' . $aoid . '" AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'AOLEVEL' => $item['AOLEVEL'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE']);
		}
		return $res;
	}

	// протестировано на Лихвинцева
	public function actionAddressByAOID($aoid)
	{
		$res= '';
		$object_address = $this->getStreetByAOID($aoid);
		if (''==$object_address)
			$object_address = $this->getObjectByAOID($aoid);
		if ($object_address && $object_address['AOLEVEL'] == 1)
		{
			return $this->getAddressByRegion($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 3)
		{
			return $this->getAddressByArea($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 4)
		{
			return $this->getAddressByCity($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 6)
		{
			return $this->getAddressByPlace($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 7)
		{
			return $this->getAddressByStreet($object_address);
		}
		return CJSON::encode($res);
	}
}
