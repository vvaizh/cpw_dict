<?php

class FIASController extends Controller {

	public $recordsAtList = 10;

	//
	/**
	 mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Удм%" AND t.AOLEVEL=1 AND t.ACTSTATUS=1 limit 10\G
*************************** 1. row ***************************
 ACTSTATUS: 1
    AOGUID: 52618b9c-bcbb-47e7-8957-95c63f0b17cc
      AOID: ae58022e-2a56-4766-a331-8d99aca4d8c4
   AOLEVEL: 1
  AREACODE: 000
  AUTOCODE: 0
CENTSTATUS: 0
  CITYCODE: 000
      CODE: 1800000000000
CURRSTATUS: 0
   ENDDATE: 2079-06-06 00:00:00
FORMALNAME: Удмуртская
    IFNSFL: 1800
    IFNSUL: 1800
    NEXTID:
   OFFNAME: Удмуртская
     OKATO: 94000000000
     OKTMO:
OPERSTATUS: 1
PARENTGUID:
 PLACECODE: 000
 PLAINCODE: 18000000000
POSTALCODE:
    PREVID:
REGIONCODE: 18
 SHORTNAME: Респ
 STARTDATE: 1900-01-01 00:00:00
STREETCODE: 0000
TERRIFNSFL:
TERRIFNSUL:
UPDATEDATE: 2011-09-13 00:00:00
  CTARCODE: 000
  EXTRCODE: 0000
  SEXTCODE: 000
LIVESTATUS: 1
   NORMDOC:
	 */
	// протестировано полностью
	public function actionRegions()
	{
		$res= array();
		if (isset($_GET['term']))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.FORMALNAME LIKE :term AND t.AOLEVEL=1 AND t.ACTSTATUS=1';
			$criteria->params= array(':term' => $_GET['term'].'%');
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res[] = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE']);
		}
		echo $this->json_encode($res);
	}

	/*
	mysql> select * from fias_addrobj where aolevel="3" and regioncode="18" and formalname like "Ал%" limit 3\G
*************************** 1. row ***************************
 ACTSTATUS: 1
    AOGUID: a09e3cff-361c-4f1c-a6c6-a5af13cead2f
      AOID: 4f05069e-9f4c-4243-9678-b06cf0f54d8b
   AOLEVEL: 3
  AREACODE: 002
  AUTOCODE: 0
CENTSTATUS: 0
  CITYCODE: 000
      CODE: 1800200000000
CURRSTATUS: 0
   ENDDATE: 2079-06-06 00:00:00
FORMALNAME: Алнашский
    IFNSFL: 1839
    IFNSUL: 1839
    NEXTID:
   OFFNAME: Алнашский
     OKATO: 94202000000
     OKTMO:
OPERSTATUS: 1
PARENTGUID: 52618b9c-bcbb-47e7-8957-95c63f0b17cc
 PLACECODE: 000
 PLAINCODE: 18002000000
POSTALCODE:
    PREVID:
REGIONCODE: 18
 SHORTNAME: р-н
 STARTDATE: 1900-01-01 00:00:00
STREETCODE: 0000
TERRIFNSFL:
TERRIFNSUL:
UPDATEDATE: 2011-09-13 00:00:00
  CTARCODE: 000
  EXTRCODE: 0000
  SEXTCODE: 000
LIVESTATUS: 1
   NORMDOC:
1 row in set (0.01 sec)
	*/
	// протестировано
	public function actionArea()
	{
		$res= array();
		if (isset($_GET['region']) && isset($_GET['area']))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=3 AND t.ACTSTATUS=1';
			$criteria->params= array(':term' => $_GET['area'].'%',':regioncode' => $_GET['region']['REGIONCODE']);
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res[] = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE']);
		}
		echo $this->json_encode($res);
	}

	/*
	mysql> select * from fias_addrobj where aolevel="6" and regioncode="18" and formalname like "Завьялово%" limit 3\G
	 *************************** 1. row ***************************
	ACTSTATUS: 1
    AOGUID: 92afdc9d-3dc4-46bd-9bf8-62b36871c0dd
	AOID: c781939a-f75f-4419-9854-50c34a55d6ff
	AOLEVEL: 6
	AREACODE: 009
	AUTOCODE: 0
	CENTSTATUS: 1
	CITYCODE: 000
	CODE: 1800900000100
	CURRSTATUS: 0
	ENDDATE: 2079-06-06 00:00:00
	FORMALNAME: Завьялово
    IFNSFL: 1841
    IFNSUL: 1841
    NEXTID:
	OFFNAME: Завьялово
	OKATO:
	OKTMO:
	OPERSTATUS: 1
	PARENTGUID: 0138fa72-5d23-4bc1-ae4f-900249c9e011
	PLACECODE: 001
	PLAINCODE: 18009000001
	POSTALCODE: 427000
    PREVID:
	REGIONCODE: 18
	SHORTNAME: с
	STARTDATE: 1900-01-01 00:00:00
	STREETCODE: 0000
	TERRIFNSFL:
	TERRIFNSUL:
	UPDATEDATE: 2012-10-05 00:00:00
	CTARCODE: 000
	EXTRCODE: 0000
	SEXTCODE: 000
	LIVESTATUS: 1
	NORMDOC:
	*/
	// протестировано
	public function actionPlace()
	{
		$res= array();
		if (isset($_GET['region']) && isset($_GET['place']))
		{
			$condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=6 AND t.ACTSTATUS=1';
			$params= array(':term' => $_GET['place'].'%',':regioncode' => $_GET['region']['REGIONCODE']);
			if (isset($_GET['city']) && $_GET['city']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.CITYCODE=:citycode';
				$params['citycode']= $_GET['city']['CITYCODE'];
			}
			if (isset($_GET['area']) && $_GET['area'])
			{
				$condition.= ' AND t.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			$criteria = new CDbCriteria;
			$criteria->params= $params;
			$criteria->condition = $condition;
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res[] = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE']);
		}
		echo $this->json_encode($res);
	}
	
		/*
	mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Ижев%" AND t.AOLEVEL=4 AND t.ACTSTATUS=1 limit 1\G
*************************** 1. row ***************************
 ACTSTATUS: 1
    AOGUID: deb1d05a-71ce-40d1-b726-6ba85d70d58f
      AOID: 873813f2-bb14-4ca2-b463-637342e7029b
   AOLEVEL: 4
  AREACODE: 000
  AUTOCODE: 0
CENTSTATUS: 2
  CITYCODE: 001
      CODE: 1800000100000
CURRSTATUS: 0
   ENDDATE: 2079-06-06 00:00:00
FORMALNAME: Ижевск
    IFNSFL:
    IFNSUL:
    NEXTID:
   OFFNAME: Ижевск
     OKATO: 94401000000
     OKTMO: 94701000
OPERSTATUS: 1
PARENTGUID: 52618b9c-bcbb-47e7-8957-95c63f0b17cc
 PLACECODE: 000
 PLAINCODE: 18000001000
POSTALCODE:
    PREVID:
REGIONCODE: 18
 SHORTNAME: г
 STARTDATE: 1900-01-01 00:00:00
STREETCODE: 0000
TERRIFNSFL:
TERRIFNSUL:
UPDATEDATE: 2012-10-05 00:00:00
  CTARCODE: 000
  EXTRCODE: 0000
  SEXTCODE: 000
LIVESTATUS: 1
   NORMDOC:
	 * */
	// протестировано
	public function actionCity()
	{
		$res= array();
		if (isset($_GET['region']) && isset($_GET['city']))
		{
			$condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=4 AND t.ACTSTATUS=1';
			$params= array(':term' => $_GET['city'].'%',':regioncode' => $_GET['region']['REGIONCODE']);
			if (isset($_GET['area']) && $_GET['area']) // Если есть район - выбираем города для текущего района
			{
				$condition.= ' AND t.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			$criteria = new CDbCriteria;
			$criteria->params= $params;
			$criteria->condition = $condition;
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res[] = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE']);
		}
		echo $this->json_encode($res);
	}

	/*
	mysql> select * from fias_addrobj t where t.FORMALNAME LIKE "Лихвинцев%" AND t.AOLEVEL=7 AND t.ACTSTATUS=1 limit 1\G
*************************** 1. row ***************************
 ACTSTATUS: 1
    AOGUID: 5a1f52ae-6930-482d-b861-fa1c943d1b85
      AOID: c87cd460-3790-4516-ab97-79dea2546ff1
   AOLEVEL: 7
  AREACODE: 000
  AUTOCODE: 0
CENTSTATUS: 0
  CITYCODE: 001
      CODE: 18000001000023900
CURRSTATUS: 0
   ENDDATE: 2079-06-06 00:00:00
FORMALNAME: Лихвинцева
    IFNSFL:
    IFNSUL:
    NEXTID:
   OFFNAME: Лихвинцева
     OKATO: 94401000000
     OKTMO: 94701000
OPERSTATUS: 1
PARENTGUID: deb1d05a-71ce-40d1-b726-6ba85d70d58f
 PLACECODE: 000
 PLAINCODE: 180000010000239
POSTALCODE:
    PREVID:
REGIONCODE: 18
 SHORTNAME: ул
 STARTDATE: 1900-01-01 00:00:00
STREETCODE: 0239
TERRIFNSFL:
TERRIFNSUL:
UPDATEDATE: 2014-04-25 00:00:00
  CTARCODE: 000
  EXTRCODE: 0000
  SEXTCODE: 000
LIVESTATUS: 1
   NORMDOC:
	 * */
	public function actionStreet()
	{
		$res= array();
		$isset_place_or_city = isset($_GET['place']) || isset($_GET['city']);
		if (isset($_GET['region']) && isset($_GET['street']) && $isset_place_or_city)
		{
			$condition = 't.FORMALNAME LIKE :term AND t.REGIONCODE=:regioncode AND t.AOLEVEL=7 AND t.ACTSTATUS=1';
			$params= array(':term' => $_GET['street'].'%',':regioncode' => $_GET['region']['REGIONCODE']);
			if (isset($_GET['area']) && $_GET['area']) // Если есть район - выбираем города для текущего района
			{
				$condition.= ' AND t.AREACODE=:areacode';
				$params['areacode']= $_GET['area']['AREACODE'];
			}
			if (isset($_GET['city']) && $_GET['city']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.CITYCODE=:citycode';
				$params['citycode']= $_GET['city']['CITYCODE'];
			}
			if (isset($_GET['place']) && $_GET['place']) // Если указан город - выбираем населенные пункты для текущего города
			{
				$condition.= ' AND t.PLACECODE=:placecode';
				$params['placecode']= $_GET['place']['PLACECODE'];
			}
			$criteria = new CDbCriteria;
			$criteria->params= $params;
			$criteria->condition = $condition;
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
			{
				$res[] = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE'],
					'FullPath' => $item['FullPath']);
			}
		}
		echo $this->json_encode($res);
	}
	
	function getRegionByCode($region_code)
	{
		$res= '';
		if (isset($region_code) && $region_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '" AND t.AOLEVEL=1 AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE']);
		}
		return $res;
	}
	
	function getAreaByCode($region_code, $area_code)
	{
		$res= '';
		if (isset($area_code) && $area_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.AREACODE="' . $area_code . '"'
				. ' AND t.AOLEVEL=3 AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE']);
		}
		return $res;
	}
	
	function getCityByCode($region_code, $area_code, $city_code)
	{
		$res= '';
		if (isset($city_code) && $city_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.CITYCODE="' . $city_code . '"'
				. ' AND t.AOLEVEL=4 AND t.ACTSTATUS=1';
			if (isset($area_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.AREACODE="' . $area_code . '"';
			}
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE']);
		}
		return $res;
	}
	
	function getPlaceByCode($region_code, $area_code, $city_code, $place_code)
	{
		$res= '';
		if (isset($place_code) && $place_code != "000")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.REGIONCODE="' . $region_code . '"'
				. ' AND t.PLACECODE="' . $place_code . '"'
				. ' AND t.AOLEVEL=6 AND t.ACTSTATUS=1';
			if (isset($area_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.AREACODE="' . $area_code . '"';
			}
			if (isset($city_code))
			{
				$criteria->condition = $criteria->condition
					. ' AND t.CITYCODE="' . $city_code . '"';
			}
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE']);
		}
		return $res;
	}
	
	public function getAddressByStreet($item)
	{
		$object_street = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE'],
			'PLACECODE' => $item['PLACECODE'],
			'STREETCODE' => $item['STREETCODE']);
		$object_region = $this->getRegionByCode($object_street['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_street['REGIONCODE'], $object_street['AREACODE']);
			$object_city = $this->getCityByCode(
				$object_street['REGIONCODE'], 
				$object_street['AREACODE'], 
				$object_street['CITYCODE']);
			$object_place = $this->getPlaceByCode(
				$object_street['REGIONCODE'], 
				$object_street['AREACODE'], 
				$object_street['CITYCODE'],
				$object_street['PLACECODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city,
			'object_place' => $object_place,
			'object_street' => $object_street
		);
		return $this->json_encode($res);
	}

	public function getAddressByPlace($item)
	{
		$object_place = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE'],
			'PLACECODE' => $item['PLACECODE']);
		$object_region = $this->getRegionByCode($object_place['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_place['REGIONCODE'], $object_place['AREACODE']);
			$object_city = $this->getCityByCode(
				$object_place['REGIONCODE'], 
				$object_place['AREACODE'], 
				$object_place['CITYCODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city,
			'object_place' => $object_place
		);
		return $this->json_encode($res);
	}

	public function getAddressByCity($item)
	{
		$object_city = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE'],
			'CITYCODE' => $item['CITYCODE']);
		$object_region = $this->getRegionByCode($object_city['REGIONCODE']);
		if ($object_region)
		{
			$object_area = $this->getAreaByCode($object_city['REGIONCODE'], $object_city['AREACODE']);
		}
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area,
			'object_city' => $object_city
		);
		return $this->json_encode($res);
	}

	public function getAddressByArea($item)
	{
		$object_area = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE'],
			'AREACODE' => $item['AREACODE']);
		$object_region = $this->getRegionByCode($object_area['REGIONCODE']);
		$res = array(
			'object_region' => $object_region,
			'object_area' => $object_area
		);
		return $this->json_encode($res);
	}

	public function getAddressByRegion($item)
	{
		$object_region = array(
			'label' => $item['label'],
			'value' => $item['value'],
			'AOID' => $item['AOID'],
			'SHORTNAME' => $item['SHORTNAME'],
			'REGIONCODE' => $item['REGIONCODE']);
		$res = array(
			'object_region' => $object_region
		);
		return $this->json_encode($res);
	}

	public function getStreetByAOID($aoid)
	{
		$res= '';
		if (isset($aoid))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.AOID=:aoid AND t.ACTSTATUS=1 AND t.AOLEVEL=7 ';
			$criteria->params= array(':aoid' => $aoid);
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'AOLEVEL' => $item['AOLEVEL'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE']);
		}
		return $res;
	}

	public function getObjectByAOID($aoid)
	{
		$res= '';
		if (isset($aoid))
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 't.AOID="' . $aoid . '" AND t.ACTSTATUS=1';
			$criteria->limit = $this->recordsAtList;
			$objects = FIASAddrobj::model()->findAll($criteria);
			foreach ($objects as $item)
				$res = array(
					'label' => $item['SHORTNAME'] . ' ' . $item['FORMALNAME'],
					'value' => $item['FORMALNAME'],
					'AOID' => $item['AOID'],
					'AOLEVEL' => $item['AOLEVEL'],
					'SHORTNAME' => $item['SHORTNAME'],
					'REGIONCODE' => $item['REGIONCODE'],
					'AREACODE' => $item['AREACODE'],
					'CITYCODE' => $item['CITYCODE'],
					'PLACECODE' => $item['PLACECODE'],
					'STREETCODE' => $item['STREETCODE']);
		}
		return $res;
	}

	// протестировано на Лихвинцева
	public function actionAddressByAOID($aoid)
	{
		$res= '';
		$object_address = $this->getStreetByAOID($aoid);
		if (''==$object_address)
			$object_address = $this->getObjectByAOID($aoid);
		if ($object_address && $object_address['AOLEVEL'] == 1)
		{
			return $this->getAddressByRegion($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 3)
		{
			return $this->getAddressByArea($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 4)
		{
			return $this->getAddressByCity($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 6)
		{
			return $this->getAddressByPlace($object_address);
		}
		else if ($object_address && $object_address['AOLEVEL'] == 7)
		{
			return $this->getAddressByStreet($object_address);
		}
		return $this->json_encode($res);
	}
}
