alter table fias_addrobj add column FullPath          varchar(255) DEFAULT NULL;
alter table fias_addrobj add index  keyAOByAOguid     (AOGUID);
alter table fias_addrobj add index  keyAOByPARENTGUID (PARENTGUID);

update fias_addrobj set FullPath=FORMALNAME where PARENTGUID is null;
