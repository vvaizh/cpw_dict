alter table fias_addrobj add index key15 (AOLEVEL,REGIONCODE,AREACODE,CITYCODE,EXTRCODE);
alter table fias_addrobj add index key16 (AOLEVEL,REGIONCODE,AREACODE,CITYCODE,EXTRCODE);
alter table fias_addrobj add index key17 (AOLEVEL,REGIONCODE,                  EXTRCODE);
alter table fias_addrobj add index key18 (AOLEVEL,REGIONCODE,         CITYCODE,EXTRCODE);
alter table fias_addrobj add index key19 (AOLEVEL,REGIONCODE,AREACODE,         EXTRCODE);
alter table fias_addrobj add index key20 (AOLEVEL,REGIONCODE,AREACODE,         EXTRCODE,FORMALNAME);