update fias_addrobj set IsTerminal=1;

update fias_addrobj ao 
	inner join fias_addrobj aoc on aoc.PARENTGUID=ao.AOGUID
set ao.IsTerminal=0;