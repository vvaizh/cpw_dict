select now() as ' ', 'drop old fias_addrobj' as ' ';
drop table if exists fias_addrobj;
select now() as ' ', 'create fias_addrobj' as ' ';
create table fias_addrobj
(
  `ACTSTATUS` int(11) DEFAULT NULL

  ,`PARENTGUID` varchar(36) DEFAULT NULL
  ,`AOGUID` varchar(36) DEFAULT NULL
  ,`AOID` varchar(36) NOT NULL
  ,`AOLEVEL` int(11) DEFAULT NULL

  ,`REGIONCODE` varchar(2) DEFAULT NULL
  ,`AREACODE` varchar(3) DEFAULT NULL
  ,`CITYCODE` varchar(3) DEFAULT NULL
  ,`PLACECODE` varchar(3) DEFAULT NULL
  ,`EXTRCODE` varchar(4) DEFAULT NULL
  ,`STREETCODE` varchar(4) DEFAULT NULL
  ,`CTARCODE` varchar(3) DEFAULT NULL
  ,`SEXTCODE` varchar(3) DEFAULT NULL

  ,`FORMALNAME` varchar(120) DEFAULT NULL
  ,`SHORTNAME` varchar(10) DEFAULT NULL

) ENGINE=MyISAM DEFAULT CHARSET=utf8;
