select now() as ' ', 'delete old data' as ' ';
delete from fias_addrobj;

select now() as ' ', 'load data from csv' as ' ';
LOAD DATA LOCAL INFILE 'addrobj.csv' INTO TABLE fias_addrobj 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
(@ACTSTATUS,
 @AOGUID,
 @AOID,
 @AOLEVEL,
 @AREACODE,
 @AUTOCODE,
 @CENTSTATUS,
 @CITYCODE,
 @CODE,
 @CURRSTATUS,
 @ENDDATE,
 @FORMALNAME,
 @IFNSFL,
 @IFNSUL,
 @NEXTID,
 @OFFNAME,
 @OKATO,
 @OKTMO,
 @OPERSTATUS,
 @PARENTGUID,
 @PLACECODE,
 @PLAINCODE,
 @POSTALCODE,
 @PREVID,
 @REGIONCODE,
 @SHORTNAME,
 @STARTDATE,
 @STREETCODE,
 @TERRIFNSFL,
 @TERRIFNSUL,
 @UPDATEDATE,
 @CTARCODE,
 @EXTRCODE,
 @SEXTCODE,
 @LIVESTATUS,
 @NORMDOC) 
SET 
  ACTSTATUS= @ACTSTATUS

  ,PARENTGUID= @PARENTGUID
  ,AOGUID= @AOGUID
  ,AOID= @AOID
  ,AOLEVEL= @AOLEVEL

  ,REGIONCODE= @REGIONCODE
  ,AREACODE= @AREACODE
  ,CITYCODE= @CITYCODE
  ,PLACECODE= @PLACECODE
  ,EXTRCODE= @EXTRCODE
  ,STREETCODE= @STREETCODE
  ,CTARCODE= @CTARCODE
  ,SEXTCODE= @SEXTCODE

  ,FORMALNAME= @FORMALNAME
  ,SHORTNAME= @SHORTNAME
;
