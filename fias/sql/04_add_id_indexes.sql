select now() as ' ', 'add index for PARENTGUID' as ' ';
alter table fias_addrobj add index  iPARENTGUID (PARENTGUID);

select now() as ' ', 'add index for AOGUID' as ' ';
alter table fias_addrobj add unique index iAOGUID (AOGUID);

select now() as ' ', 'add index for AOID' as ' ';
alter table fias_addrobj add unique index  iAOID (AOID);

