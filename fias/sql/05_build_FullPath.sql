select now() as ' ', 'update FullPath for PARENTGUID is null' as ' ';
update fias_addrobj set FullPath=null where PARENTGUID is null || aolevel=1;

select now() as ' ', 'update FullPath for aolevel=3' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=3;

select now() as ' ', 'update FullPath for aolevel=4' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=4;

select now() as ' ', 'update FullPath for aolevel=5' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=5;

select now() as ' ', 'update FullPath for aolevel=6' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=6;

select now() as ' ', 'update FullPath for aolevel=7' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=7;

select now() as ' ', 'update FullPath for aolevel=8' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=90;

select now() as ' ', 'update FullPath for aolevel=9' as ' ';
update fias_addrobj fa left join fias_addrobj p on fa.parentguid=p.aoguid
set fa.FullPath=if(p.FullPath is null,concat(p.shortname,' ',p.FORMALNAME),concat(p.FullPath,'$',p.shortname,' ',p.FORMALNAME)) 
where fa.aolevel=91;
