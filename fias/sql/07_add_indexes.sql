select now() as ' ', 'add is1' as ' ';
alter table fias_addrobj add index is1  (                                                                    FORMALNAME,AOLEVEL);
select now() as ' ', 'add is1_0' as ' ';
alter table fias_addrobj add index is1_0(                                                                              AOLEVEL);

select now() as ' ', 'add is2' as ' ';
alter table fias_addrobj add index is2  (REGIONCODE,                                                         FORMALNAME,AOLEVEL);
select now() as ' ', 'add is2_0' as ' ';
alter table fias_addrobj add index is2_0(REGIONCODE,                                                                    AOLEVEL);

select now() as ' ', 'add is3' as ' ';
alter table fias_addrobj add index is3  (REGIONCODE,AREACODE,                                                FORMALNAME,AOLEVEL);
select now() as ' ', 'add is3_0' as ' ';
alter table fias_addrobj add index is3_0(REGIONCODE,AREACODE,                                                           AOLEVEL);

select now() as ' ', 'add is4' as ' ';
alter table fias_addrobj add index is4  (REGIONCODE,AREACODE,CITYCODE,                                       FORMALNAME,AOLEVEL);
select now() as ' ', 'add is4_0' as ' ';
alter table fias_addrobj add index is4_0(REGIONCODE,AREACODE,CITYCODE,                                                  AOLEVEL);

select now() as ' ', 'add is5' as ' ';
alter table fias_addrobj add index is5  (REGIONCODE,AREACODE,CITYCODE,PLACECODE,                             FORMALNAME,AOLEVEL);
select now() as ' ', 'add is5_0' as ' ';
alter table fias_addrobj add index is5_0(REGIONCODE,AREACODE,CITYCODE,PLACECODE,                                        AOLEVEL);

select now() as ' ', 'add is6' as ' ';
alter table fias_addrobj add index is6  (REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,                    FORMALNAME,AOLEVEL);
select now() as ' ', 'add is6_0' as ' ';
alter table fias_addrobj add index is6_0(REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,                               AOLEVEL);

select now() as ' ', 'add is7' as ' ';
alter table fias_addrobj add index is7  (REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,STREETCODE,         FORMALNAME,AOLEVEL);
select now() as ' ', 'add is7_0' as ' ';
alter table fias_addrobj add index is7_0(REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,STREETCODE,                    AOLEVEL);

select now() as ' ', 'add is8' as ' ';
alter table fias_addrobj add index is8  (REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,STREETCODE,CTARCODE,FORMALNAME,AOLEVEL);
select now() as ' ', 'add is8_0' as ' ';
alter table fias_addrobj add index is8_0(REGIONCODE,AREACODE,CITYCODE,PLACECODE,EXTRCODE,STREETCODE,CTARCODE,           AOLEVEL);

select now() as ' ', 'add order by FORMALNAME' as ' ';
alter table fias_addrobj order by FORMALNAME;
select now() as ' ', 'analyze table..' as ' ';
analyze table fias_addrobj;


