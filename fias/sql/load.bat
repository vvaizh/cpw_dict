@rem @echo %time% create table
@rem @call run_mysql.bat < 00_create.sql

@rem @echo %time% load csv
@rem @call run_mysql.bat < 01_load.sql

@rem @echo %time% filter actual
@rem @call run_mysql.bat < 02_filter_actual.sql

@rem @echo %time% add FullPath, IsTerminal
@rem @call run_mysql.bat < 03_add_extra_fields.sql

@rem @echo %time% add id indexes
@rem @call run_mysql.bat < 04_add_id_indexes.sql

@echo %time% build full path
@call run_mysql.bat < 05_build_FullPath.sql

@rem @echo %time% build is terminal
@rem @call run_mysql.bat < 06_build_IsTerminal.sql

@rem @echo %time% build indexes to search
@rem @call run_mysql.bat < 07_add_indexes.sql

@echo %time%
