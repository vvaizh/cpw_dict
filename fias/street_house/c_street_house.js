define([
	  'forms/fms/base/town/c_town'
	, 'forms/fms/base/h_Select2'
	, 'tpl!forms/fms/base/street_house/e_street_house.html'
	, 'forms/fms/base/town/h_town'
	, 'forms/fms/base/street_house/h_street_house'
	, 'forms/base/h_validate'
],
function (controller_base, helper_Select2, address_street_house_tpl, helper_town, helper_street_house, helper_validate)
{
	return function (model, title, OnAfterSave)
	{
		var controller = controller_base(model, OnAfterSave);

		controller.title = title;
		controller.width = 750;
		controller.height = 400;

		var checkNull = function (param)
		{
			return param ? param.id : '';
		}

		var base_template = controller.template;
		controller.template = function (m)
		{
			var res = '<div>';
			res += base_template(m);
			res += address_street_house_tpl(m);
			res += '</div>';
			return res;
		};

		var formatStreet = function (item)
		{
			var street = item.text;
			if (!item.FullPath)
			{
				return street;
			}
			else
			{
				var addr_txt = item.FullPath.replace('$' + street, '').replace(/\$/g, ', ');
				return '<div><span>' + street + '</span><div class="cpw-arrd-street-spec">(' + addr_txt + ')</div></div>';
			}
		}

		controller.Select2StreetsWithDefaults = function (input, action, result, data)
		{
			input.select2({
				placeholder: '',
				allowClear: true,
				minimumInputLength: 1,
				ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
					url: "https://probili.ru/fias/" + action,
					type: 'GET',
					dataType: 'jsonp',
					quietMillis: 500,
					data: data,
					results: result,
					cache: true
				},
				dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
				formatResult: formatStreet,
				formatSelection: function (item) { return item.text; }
			}).on('change', function ()
			{
				controller.Select2ClearValues($(this).attr('id'));
			})
		}

		var base_InitializeControls = controller.InitializeControls;
		controller.InitializeControls = function ()
		{
			base_InitializeControls();

			controller.Select2StreetsWithDefaults
			(
				$('#cpw_form_modal_Street'),
				'street',
				function (data, page)
				{
					var res = [];
					for (var i = 0; i < data.length; i++)
					{
						var row = data[i];
						res.push({ id: row.AOID, text: row.label, FullPath: row.FullPath });
					}
					return { results: res };
				},
				function (term, page)
				{
					return {
						'region[REGIONCODE]': checkNull($('#cpw_form_modal_Region').select2('data')),
						'area[AREACODE]': checkNull($('#cpw_form_modal_Area').select2('data')),
						'city[CITYCODE]': checkNull($('#cpw_form_modal_City').select2('data')),
						'place[PLACECODE]': checkNull($('#cpw_form_modal_Town').select2('data')),
						street: term
					};
				}
			);

			controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Street');

			$('#cpw_form_modal_HouseNumber').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_KorpusNumber').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_FlatNumber').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
		}

		var base_DataLoadToControls = controller.DataLoadToControls;
		controller.DataLoadToControls = function ()
		{
			base_DataLoadToControls();
			$('#cpw_form_modal_Street').select2('data', (!controller.model || !controller.model.Street) ? null : controller.model.Street);

			$('#cpw_form_modal_HouseNumber').val(controller.model.HouseNumber);
			$('#cpw_form_modal_KorpusNumber').val(controller.model.KorpusNumber);
			$('#cpw_form_modal_FlatNumber').val(controller.model.FlatNumber);
		}

		var base_DataSaveFromControls = controller.DataSaveFromControls;
		controller.DataSaveFromControls = function ()
		{
			base_DataSaveFromControls();
			this.model.Street = $('#cpw_form_modal_Street').select2('data');

			this.model.HouseNumber = $('#cpw_form_modal_HouseNumber').val();
			this.model.KorpusNumber = $('#cpw_form_modal_KorpusNumber').val();
			this.model.FlatNumber = $('#cpw_form_modal_FlatNumber').val();

			this.model.text = helper_street_house.ToReadableStringOrText(model, null);
		}

		controller.Validate = function ()
		{
			var res1 = $('#cpw_form_modal_Street').select2('data') != '';
			var res2 = helper_validate.ValidateIsNotEmpty($('#cpw_form_modal_Street'));
			return res1 || res2;
		}

		return controller;
	}
});
