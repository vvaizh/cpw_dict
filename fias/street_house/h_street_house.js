define(
['forms/fms/base/town/h_town'],
function (helper_town)
{
	var res = {};

	res.ToReadableString = function (m_street_house)
	{
		var txt = helper_town.ToReadableString(m_street_house);
		if (m_street_house.Street.text)
			txt += ', ' + m_street_house.Street.text;
		if (m_street_house.HouseNumber)
			txt += ', д. ' + m_street_house.HouseNumber;
		if (m_street_house.KorpusNumber)
			txt += ', корп. ' + m_street_house.KorpusNumber;
		if (m_street_house.FlatNumber)
			txt += ', кв. ' + m_street_house.FlatNumber;
		return txt;
	}

	res.ToReadableStringOrText = function (m_street_house, text)
	{
		if (!m_street_house.Street)
		{
			return text
		}
		else
		{
			return this.ToReadableString(m_street_house);
		}
	}

	res.Streets =
	[
		{ id: 1, text: 'Лихвинцева' },
		{ id: 2, text: 'Коммунаров' },
		{ id: 3, text: 'Пушкинская' },
	];

	return res;
});
