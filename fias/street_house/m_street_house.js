define([
	'forms/fms/base/town/m_town'
],
function (TownAddress)
{
	return function ()
	{
		var res = TownAddress();

		res.Street = null; // {id:'',text:''}
		res.HouseNumber = '';
		res.KorpusNumber = '';
		res.FlatNumber = '';

		return res;
	}
});
