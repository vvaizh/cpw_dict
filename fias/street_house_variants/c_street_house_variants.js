define([
	  'forms/fms/base/street_house/c_street_house'
	, 'forms/fms/base/h_Select2'
	, 'tpl!forms/fms/base/street_house_variants/v_street_house_variants.html'
],
function (controller_base, helper_Select2, address_street_house_variants_tpl)
{
	return function (model, title, OnAfterSave, variants)
	{
		var controller = controller_base(model, title, OnAfterSave);

		controller.width = 750;
		controller.height = 460;

		var variants_address = [];
		for (var i = 0; i < variants.length; i++)
		{
			variants[i].id = i;
			variants_address.push(variants[i]);
		}

		var base_template = controller.template;
		controller.template = function (m)
		{
			var res = '<div>';
			res += address_street_house_variants_tpl(m);
			res += base_template(m);
			res += '</div>';
			return res;
		};

		var clone = function (obj)
		{
			if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj)
				return obj;

			var temp = obj.constructor(); // changed

			for (var key in obj)
			{
				if (Object.prototype.hasOwnProperty.call(obj, key))
				{
					obj['isActiveClone'] = null;
					temp[key] = clone(obj[key]);
					delete obj['isActiveClone'];
				}
			}
			return temp;
		};

		var base_InitializeControls = controller.InitializeControls;
		var base_DataLoadToControls = controller.DataLoadToControls;
		controller.InitializeControls = function ()
		{
			base_InitializeControls();

			var select2_Variants = $('#cpw_form_modal_Variants');
			select2_Variants.select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{
					query.callback({ results: helper_Select2.FindForSelect(variants_address, query.term) });
				}
			});
			select2_Variants.on("select2-selecting", function (e)
			{
				var temp = controller.model;
				var obj = e.object;

				for (var key in obj)
				{
					if (Object.prototype.hasOwnProperty.call(obj, key))
					{
						obj['isActiveClone'] = null;
						temp[key] = clone(obj[key]);
						delete obj['isActiveClone'];
					}
				}

				base_DataLoadToControls();
			});

			controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Variants');
			$('#s2id_cpw_form_modal_Variants .select2-focusser').focus();
		}

		return controller;
	}
});
