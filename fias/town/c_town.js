define([
	  'forms/base/c_modal'
	, 'forms/fms/base/h_Select2'
	, 'tpl!forms/fms/base/town/e_town.html'
	, 'forms/fms/base/town/h_town'
    , 'forms/base/log'
],
function (controller_base, helper_Select2, address_town_tpl, helper_town, GetLogger)
{
    var log = GetLogger('c_town');
	return function (model, OnAfterSave)
	{
		var controller = controller_base(model);

		controller.width = 750;
		controller.height = 300;
		controller.title = 'Место рождения';

		controller.template = address_town_tpl;
		controller.OnAfterSave = OnAfterSave;

		var checkNull = function (param) {
			return param ? param.id : '';
		}

		controller.Select2WithDefaults = function (input, action, result, data) 
		{
			input.select2({
				placeholder: '',
				allowClear: true,
				minimumInputLength: 1,
			    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
			        url: "https://probili.ru/fias/" + action,
			        type: 'GET',
			        dataType: 'jsonp',
			        quietMillis: 500,
			        data: data,
			        results: result, 
			        cache: true
			    },
			    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
			    formatResult: function (item) { return item.text; },
			    formatSelection: function (item) { return item.text; }
			}).on('change', function () {
				log.Debug('on change Select2WithDefaults {');
				controller.Select2ClearValues($(this).attr('id'));
				log.Debug('on change Select2WithDefaults }');
			})
		}

		controller.Select2ClearValues = function (input_id)
		{
			var clear = 0;
			var input_collection = $('.cpw_form_modal_Address').find('input');
			input_collection.each(function () {
				var el = $(this);
				if(el.attr('id') != undefined) {
					if (clear == 1) {
						if (el.attr('tabindex') == -1)
							el.select2('data', '');
						else
							$(this).val('')
					}
					if (el.attr('id') == input_id) clear = 1;
				}
			});
		}

		controller.InitializeControls = function ()
		{
			controller.InitializeFields();
			controller.InitializeEvents();
		}

		controller.InitializeFields = function ()
		{
			// Переделать на код ОКСМ ALPHA3
			$('#cpw_form_modal_Country').select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{	// query.term..
					query.callback({ results: helper_Select2.FindForSelect(helper_town.GetValues(), query.term) });
				}
			}).on("select2-selecting", function(e) {
				controller.Select2ClearValues($(this).attr('id'));
			});
			// return false for Birthplace
			if (controller.model.HouseNumber !== undefined)
			{
				controller.Select2WithDefaults(
					$('#cpw_form_modal_Region'), 
					'regions', 
					function (data, page) {
						var res = [];
						for (var i = 0; i < data.length; i++) {
			        		res.push({id: data[i].REGIONCODE, text: data[i].label});
						}
						return { results: res };
					},
					function (term, page) {
						return {
							term: term // search term
						};
					}	
				);

				controller.Select2WithDefaults(
					$('#cpw_form_modal_Area'), 
					'area', 
					function (data, page) {
						var res = [];
						for (var i = 0; i < data.length; i++) {
			        		res.push({id: data[i].AREACODE, text: data[i].label});
						}
						return { results: res };
					},
					function (term, page) {
						return {
							'region[REGIONCODE]': checkNull($('#cpw_form_modal_Region').select2('data')),
							area: term
						};
					}	
				);

				controller.Select2WithDefaults(
					$('#cpw_form_modal_City'), 
					'city', 
					function (data, page) {
						var res = [];
						for (var i = 0; i < data.length; i++) {
			        		res.push({id: data[i].CITYCODE, text: data[i].label, region: {id: data[i].REGIONCODE, text: data[i].REGIONNAME}});
						}
						return { results: res };
					},
					function (term, page) {
						return {
							'region[REGIONCODE]': checkNull($('#cpw_form_modal_Region').select2('data')),
							'area[AREACODE]': checkNull($('#cpw_form_modal_Area').select2('data')), 
							city: term 
						};
					}	
				);
				$('#cpw_form_modal_City').on('change', function () {
					var val = $(this).select2('data');
					if (val && val.region)
						$('#cpw_form_modal_Region').select2('data', val.region);
				});

				controller.Select2WithDefaults(
					$('#cpw_form_modal_Town'), 
					'place', 
					function (data, page) {
						var res = [];
						for (var i = 0; i < data.length; i++) {
			        		res.push({id: data[i].PLACECODE, text: data[i].label});
						}
						return { results: res };
					},
					function (term, page) {
						return {
							'region[REGIONCODE]': checkNull($('#cpw_form_modal_Region').select2('data')),
							'area[AREACODE]': checkNull($('#cpw_form_modal_Area').select2('data')), 
							'city[CITYCODE]': checkNull($('#cpw_form_modal_City').select2('data')), 
							place: term
						};
					}
				);
			}
		}

		controller.InitializeEvents = function ()
		{
			controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Country');
			if (undefined != controller.model.HouseNumber)
			{
				$('#cpw_form_modal_Region').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
				$('#cpw_form_modal_Area').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
				$('#cpw_form_modal_Town').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
				$('#cpw_form_modal_City').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
				$('#cpw_form_modal_City').focus();
			}
			else
			{
				controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Region');
				controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Area');
				controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_Town');
				controller.CloseOnCtrlEnterForSelect2('cpw_form_modal_City');

				$('#s2id_cpw_form_modal_City .select2-focusser').focus();
				$('#cpw_form_modal_Address').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			}
		}

		controller.DestroyControls = function ()
		{
			controller.DestroySelect2('#cpw_form_modal_Country');
			controller.DestroySelect2('#cpw_form_modal_Region');
			controller.DestroySelect2('#cpw_form_modal_Area');
			controller.DestroySelect2('#cpw_form_modal_Town');
			controller.DestroySelect2('#cpw_form_modal_City');
			controller.DestroySelect2('#cpw_form_modal_Street');
			controller.DestroySelect2('#cpw_form_modal_Variants');
		}

		controller.DataSaveFromControls = function ()
		{
			log.Debug('DataSaveFromControls {');
			if (undefined !== controller.model.HouseNumber)
			{
				controller.model.Country = $('#cpw_form_modal_Country').select2('data');
				controller.model.Region = $('#cpw_form_modal_Region').select2('data');
				controller.model.Area = $('#cpw_form_modal_Area').select2('data');
				controller.model.City = $('#cpw_form_modal_City').select2('data');
				controller.model.Town = $('#cpw_form_modal_Town').select2('data');
				controller.model.text = helper_town.ToReadableStringOrText(model, null);
			}
			else
			{
				controller.model.Country = $('#cpw_form_modal_Country').select2('data');
				controller.model.Region = $('#cpw_form_modal_Region').val();
				controller.model.Area = $('#cpw_form_modal_Area').val();
				controller.model.City = $('#cpw_form_modal_City').val();
				controller.model.Town = $('#cpw_form_modal_Town').val();
				controller.model.text = helper_town.ToReadableStringBirthplace(this.model);
			}
			log.Debug('DataSaveFromControls }');
		}

		controller.DataLoadToControls = function ()
		{
			if (undefined !== controller.model.HouseNumber)
			{
				$('#cpw_form_modal_Country').select2('data', (!controller.model || !controller.model.Country) ? {id: 'RUS', text: 'Россия'} : controller.model.Country);
				$('#cpw_form_modal_Region').select2('data', (!controller.model || !controller.model.Region) ? null : controller.model.Region);
				$('#cpw_form_modal_Area').select2('data', (!controller.model || !controller.model.Area) ? null : controller.model.Area);
				$('#cpw_form_modal_City').select2('data', (!controller.model || !controller.model.City) ? null : controller.model.City);
				$('#cpw_form_modal_Town').select2('data', (!controller.model || !controller.model.Town) ? null : controller.model.Town);
			}
			else
			{
				$('#cpw_form_modal_Country').select2('data', (!controller.model || !controller.model.Country) ? {id: 'RUS', text: 'Россия'} : controller.model.Country);
				$('#cpw_form_modal_Region').val((!controller.model || !controller.model.Region) ? null : controller.model.Region);
				$('#cpw_form_modal_Area').val((!controller.model || !controller.model.Area) ? null : controller.model.Area);
				$('#cpw_form_modal_City').val((!controller.model || !controller.model.City) ? null : controller.model.City);
				$('#cpw_form_modal_Town').val((!controller.model || !controller.model.Town) ? null : controller.model.Town);
			}
		}

		return controller;
	}
});
