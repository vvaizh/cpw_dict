// утф8
define([
	'txt!forms/fms/guides/dict_country.csv',
	'forms/base/h_csv'
]
, function (file_CSV, helper_CSV)
{
	var res = {};

	res.ToReadableString = function (m_town)
	{
		var txt = '';
		if (m_town.Country && m_town.Country.text)
			txt = m_town.Country.text;
		return this.AppendRussianReadableString(txt,m_town);
	}

	res.AppendRussianReadableString = function (txt, m_town)
	{
		if (m_town.Region && m_town.Region.text)
		{
			if ('' != txt)
				txt += ', ';
			txt += m_town.Region.text;
		}
		if (m_town.Area && m_town.Area.text)
		{
			if ('' != txt)
				txt += ', ';
			txt += m_town.Area.text;
		}
		if (m_town.City && m_town.City.text)
			txt += ', ' + m_town.City.text;
		if (m_town.Town && m_town.Town.text)
		{
			if ('' != txt)
				txt += ', ';
			txt += m_town.Town.text;
		}
		return txt;
	}

	res.ToRussianReadableString = function (m_town)
	{
		return this.AppendRussianReadableString('', m_town); ;
	}

	res.ToReadableStringBirthplace = function(model)
	{
		var string = '';
		string += !model.Country ? '' : model.Country.text;
		string += model.Region ? (', ' + model.Region) : '';
		string += model.Area ? (', ' + model.Area) : '';
		string += model.City ? (', ' + model.City) : '';
		string += model.Town ? (', ' + model.Town) : '';
		return string;
	}

	res.ToReadableStringOrText = function (m_town, text)
	{
		if (!m_town.City && !m_town.Town)
		{
			return text
		}
		else
		{
			return this.ToReadableString(m_town);
		}
	}

	res.GetValues = function ()
	{
		return helper_CSV.CSVToJSON(file_CSV, ';');
	}

	res.FindRowById = function (list, id)
	{
		for (var i = 0; i < list.length; i++)
		{
			var row = list[i];
			if (id == row.id)
				return row;
		}
		return null;
	}

	res.SetCity = function (m_town, new_city)
	{
		m_town.City = new_city;
		var city = this.FindRowById(this.Cities, new_city.id);
		if (null != city)
		{
			m_town.Area = this.FindRowById(this.Areas, city.id_Area);
			m_town.Region = this.FindRowById(this.Regions, city.id_Region);
			m_town.Country = this.FindRowById(this.Countries, city.id_Country);
		}
	}

	res.CopyFromTo = function (from, to)
	{
		if (from != null && to != null)
		{
			to.Town = from.Town;
			to.City = from.City;
			to.Area = from.Area;
			to.Region = from.Region;
			to.Country = from.Country;
		}
	}

	return res;
});
