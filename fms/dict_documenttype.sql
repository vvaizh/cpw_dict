delete from dict_documenttype;
LOAD DATA LOCAL INFILE 'dictionaries\\dict_documenttype.csv' INTO TABLE dict_documenttype 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
alter table dict_documenttype order by NAME;
analyze table dict_documenttype;