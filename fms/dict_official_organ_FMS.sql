delete from dict_official_organ_FMS;
LOAD DATA LOCAL INFILE 'dictionaries\\dict_official_organ_FMS.csv' INTO TABLE dict_official_organ_FMS 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(ID, NAME, CODE, @END_DATE)
SET END_DATE = IF(@END_DATE="", NULL, STR_TO_DATE(@END_DATE,'%d.%m.%Y'));
alter table dict_official_organ_FMS order by NAME;
analyze table dict_official_organ_FMS;