delete from dict_official_organ_FNS;
LOAD DATA LOCAL INFILE 'dictionaries\\dict_official_organ_FNS.csv' INTO TABLE dict_official_organ_FNS 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
alter table dict_official_organ_FNS order by NAME;
analyze table dict_official_organ_FNS;