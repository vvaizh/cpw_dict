delete from dict_official_organ_KPP;
LOAD DATA LOCAL INFILE 'dictionaries\\dict_official_organ_KPP.csv' INTO TABLE dict_official_organ_KPP 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
alter table dict_official_organ_KPP order by NAME;
analyze table dict_official_organ_KPP;