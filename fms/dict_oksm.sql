delete from dict_oksm;
LOAD DATA LOCAL INFILE 'dictionaries\\dict_oksm.csv' INTO TABLE dict_oksm 
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
alter table dict_oksm order by SHORTNAME;
analyze table dict_oksm;