﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Reflection;

namespace dbf
{
    public class ConverterDBFtoCSV
    {
        public static void ConvertDBFtoCSV(string[] args)
        {
            string currentDirectory = Directory.GetCurrentDirectory();

            FileInfo dbfFileInfo = new FileInfo(args[0]);
            string filepath = dbfFileInfo.DirectoryName;
            string dbfFile = dbfFileInfo.Name;
            string csvFile = Path.Combine(dbfFileInfo.DirectoryName, dbfFileInfo.Name.ToLower().Replace("dbf", "csv"));
            Console.WriteLine("{0}: Конвертируем файл {1} в csv", DateTime.Now.ToString(), args[0]);
            DataTable dataTable = new DataTable();
            string connectionString =
                "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=dBASE IV;Data Source='" + filepath + "'";
            OleDbConnection oledbConnection = new OleDbConnection(connectionString);
            try
            {
                FileStream fs = new FileStream(csvFile, FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(fs);
                oledbConnection.Open();
                int countRowsDBF = GetCountRows(connectionString, dbfFile);
                string stringCommadn = string.Format(@"select * from " + dbfFile);
                OleDbCommand oleDbCommand = new OleDbCommand(stringCommadn, oledbConnection);
                OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter
                {
                    SelectCommand = oleDbCommand
                };
                int position = 0;
                int countRows = 0;
                do
                {
                    countRows = oleDbDataAdapter.Fill(position, 1000, dataTable);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DataRow rows = dataTable.Rows[i];
                        object row = rows.ItemArray.FirstOrDefault(ob => ob is DateTime);
                        sw.WriteLine("\"{0}\"", string.Join("\";\"", rows.ItemArray.Select(ob => 
                            ob is DateTime ? ((DateTime)ob).ToShortDateString() : 
                            ob is string ? ((string)ob).Replace("\"", "\\\"") : ob)));
                    }
                    sw.Flush();
                    dataTable.Rows.Clear();
                    position += countRows;
                    if (countRows > 0)
                        Console.WriteLine("{0}: Прочитано {1} строк из {2}", DateTime.Now.ToString(), position, countRowsDBF);
                } while (countRows > 0);
                sw.Close();
                Console.WriteLine("{0}: Данные сохранены в файл {1}", DateTime.Now.ToString(), csvFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                oledbConnection.Close();
            }
        }

        static int GetCountRows(string connectionString, string dbfFile)
        {
            int countRowsDBF = 0;
            DataTable dataTable = new DataTable();
            OleDbConnection oledbConnection = new OleDbConnection(connectionString);
            try
            {
                string stringCommadn = string.Format(@"select count(*) from " + dbfFile);
                OleDbCommand oleDbCommand = new OleDbCommand(stringCommadn, oledbConnection);
                OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter
                {
                    SelectCommand = oleDbCommand
                };
                int c = oleDbDataAdapter.Fill(dataTable);
                countRowsDBF = dataTable.Rows.Count > 0 && dataTable.Rows[0].ItemArray.Length > 0 ?
                    (int)dataTable.Rows[0].ItemArray.First() : 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                oledbConnection.Close();
            }
            return countRowsDBF;
        }
    }
}
