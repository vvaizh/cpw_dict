﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Reflection;

namespace dbf
{
    public class KLADR
    {
        public static void ParseDBF()
        {
            string currentDirectory = Directory.GetCurrentDirectory();

            string kladrFile = currentDirectory + @"\KLADR\KLADR.DBF";
            string streetFile = currentDirectory + @"\KLADR\STREET.DBF";

            string SqlFile1 = currentDirectory + @"\KLADR\region.sql";
            string SqlFile2 = currentDirectory + @"\KLADR\district.sql";
            string SqlFile3 = currentDirectory + @"\KLADR\city.sql";
            string SqlFile4 = currentDirectory + @"\KLADR\settlement.sql";
            string SqlFile5 = currentDirectory + @"\KLADR\street.sql";
            OdbcConnection Conn = new System.Data.Odbc.OdbcConnection();
            Conn.ConnectionString = @"Driver={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=c:\;";
            DataTable dt = new DataTable();
            if (Conn != null)
            {
                try
                {
                    int type;
                    string code;
                    string query;
                    FileStream FS1 = new FileStream(SqlFile1, FileMode.OpenOrCreate);
                    StreamWriter SW1 = new StreamWriter(FS1);
                    FileStream FS2 = new FileStream(SqlFile2, FileMode.OpenOrCreate);
                    StreamWriter SW2 = new StreamWriter(FS2);
                    FileStream FS3 = new FileStream(SqlFile3, FileMode.OpenOrCreate);
                    StreamWriter SW3 = new StreamWriter(FS3);
                    FileStream FS4 = new FileStream(SqlFile4, FileMode.OpenOrCreate);
                    StreamWriter SW4 = new StreamWriter(FS4);
                    FileStream FS5 = new FileStream(SqlFile5, FileMode.OpenOrCreate);
                    StreamWriter SW5 = new StreamWriter(FS5);
                    Conn.Open();
                    Console.WriteLine();
                    System.Data.Odbc.OdbcCommand oCmd = Conn.CreateCommand();
                    oCmd.CommandText = "SELECT * FROM " + kladrFile;
                    dt.Load(oCmd.ExecuteReader());
                    int count1 = 0;
                    int count2 = 0;
                    int count3 = 0;
                    int count4 = 0;
                    int strLen = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        code = dt.Rows[i][2].ToString();
                        if (code.Substring(11, 2) == "00")
                        {
                            type = typeOfObject(code);
                            if (type == 1)
                            {
                                query = "(" + getRegionByCode(code) + ", \"" + dt.Rows[i][0].ToString() + "\", \"" + dt.Rows[i][1].ToString() + "\")";
                                if (count1 == 0)
                                {
                                    SW1.Write("INSERT INTO region (id, Name, Short) VALUES " + query);
                                    count1++;
                                }
                                else
                                {
                                    SW1.Write(", " + query);
                                }
                            }
                            else if (type == 2)
                            {
                                query = "(" + getDistrictByCode(code) + ", \"" + dt.Rows[i][0].ToString() + "\", \"" + dt.Rows[i][1].ToString() + "\", " + getRegionByCode(code) + ")";
                                if (count2 == 0)
                                {
                                    SW2.Write("INSERT INTO district (id, Name, Short, Region_id) VALUES " + query);
                                    count2++;
                                }
                                else
                                {
                                    SW2.Write(", " + query);
                                }
                            }
                            else if (type == 3)
                            {
                                query = "(" + getCityByCode(code) + ", \"" + dt.Rows[i][0].ToString() + "\", \"" + dt.Rows[i][1].ToString() + "\", \"" + dt.Rows[i][3].ToString() + "\", " + getRegionByCode(code) + ", " + getDistrictByCode(code) + ")";
                                if (count3 == 0)
                                {
                                    SW3.Write("INSERT INTO city (id, Name, Short, pIndex, Region_id, District_id) VALUES " + query);
                                    count3++;
                                }
                                else
                                {
                                    SW3.Write(", " + query);
                                }
                            }
                            else if (type == 4)
                            {
                                query = "(" + getSettlementByCode(code) + ", \"" + dt.Rows[i][0].ToString().Replace("\"", "\\\"") + "\", \"" + dt.Rows[i][1].ToString().Replace("\"", "\\\"") + "\", \"" + dt.Rows[i][3].ToString() + "\", " + getRegionByCode(code) + ", " + getDistrictByCode(code) + ", " + getCityByCode(code) + ")";
                                if (count4 == 0)
                                {
                                    SW4.Write("INSERT INTO settlement (id, Name, Short, pIndex, Region_id, District_id, City_id) VALUES " + query);
                                    strLen += (query.Length + 100);
                                    count4++;
                                }
                                else
                                {
                                    strLen += query.Length;
                                    SW4.Write(", " + query);
                                }
                                if (strLen > 5000)
                                {
                                    SW4.Write(";\n");
                                    strLen = 0;
                                    count4 = 0;
                                }
                            }
                        }
                    }
                    SW1.Close();
                    SW2.Close();
                    SW3.Close();
                    SW4.Close();
                    oCmd.CommandText = "SELECT * FROM " + streetFile;
                    DataTable dt2 = new DataTable();
                    dt2.Load(oCmd.ExecuteReader());
                    count1 = 0;
                    strLen = 0;
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        code = dt2.Rows[i][2].ToString();
                        if (code.Substring(15, 2) == "00")
                        {
                            query = "(" + getStreetByCode(code) + ", \"" + dt2.Rows[i][0].ToString().Replace("\"", "\\\"") + "\", \"" + dt2.Rows[i][1].ToString().Replace("\"", "\\\"") + "\", " + getCityByCode(code) + ", " + getSettlementByCode(code) + ")";
                            if (count1 == 0)
                            {
                                SW5.Write("INSERT INTO street (id, Name, Short, City_id, Settlement_id) VALUES " + query);
                                strLen += (query.Length + 100);
                                count1++;
                            }
                            else
                            {
                                strLen += query.Length;
                                SW5.Write(", " + query);
                            }
                            if (strLen > 20000)
                            {
                                SW5.Write(";\n");
                                strLen = 0;
                                count1 = 0;
                            }
                        }
                    }
                    SW5.Close();
                    Conn.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static int getRegionByCode(string code)
        {
            return int.Parse(code.Substring(0, 2));
        }

        public static int getDistrictByCode(string code)
        {
            return int.Parse(code.Substring(0, 5));
        }

        public static int getCityByCode(string code)
        {
            return int.Parse(code.Substring(0, 8));
        }

        public static Int64 getSettlementByCode(string code)
        {
            return Int64.Parse(code.Substring(0, 11));
        }

        public static Int64 getStreetByCode(string code)
        {
            return Int64.Parse(code.Substring(0, 15));
        }

        public static int typeOfObject(string code)
        {
            if (code.Substring(8, 3) != "000")
            {
                return 4;
            }
            else if (code.Substring(5, 3) != "000")
            {
                return 3;
            }
            else if (code.Substring(2, 3) != "000")
            {
                return 2;
            }
            return 1;
        }
    }
}
