﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Reflection;


namespace dbf
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                KLADR.ParseDBF();
            }
            else
            {
                ConverterDBFtoCSV.ConvertDBFtoCSV(args);
            }
        }
    }
}